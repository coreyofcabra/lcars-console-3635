<!doctype html>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>LCARS</title>
  <link rel="stylesheet" href="css/lcars.css">
  <link rel="stylesheet" href="css/custom.css">
  
<?php

date_default_timezone_set("America/Detroit");
$locationID="5015618";//Wyoming, MI
$url="http://api.openweathermap.org/data/2.5/weather?APPID=ce11f65170534f38e0d3840026c695c4&id=".$locationID."&units=metric&cnt=7&lang=en";
$json=file_get_contents($url);
$data=json_decode($json,true);

?>

  <style>
  html, body { background: black }
  </style>
</head>
<body>

  <div class="lcars-container">

    <!-- TOP ROW
    =========================================================== -->
    <div class="lcars-row spaced">

      <!-- TOP LEFT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow left bottom lcars-blue-bg">
        <a href="index.php">Notice A</a>
      </div>

      <!-- TOP DIVIDER AND BUTTONS
      =========================================================== -->
      <div class="lcars-column u-6-8 lcars-divider lcars-blue-tan-divide">
        <div class="lcars-row">
          <div class="lcars-column u-1-2">
            WEATHER
          </div>
          <div class="lcars-column u-1-2">
            <h1 class="right">3635</h1>
          </div>
        </div>
      </div>

      <!-- TOP RIGHT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow right bottom lcars-tan-bg">
        <a href="#">Side Button 2</a>
      </div>
    </div>

    <div class="lcars-row">

      <!-- LEFT MENU
      =========================================================== -->
      <div class="lcars-column u-1-8">
        <ul class="lcars-menu">
          <li class="lcars-blue-bg"><a href="index-1.php">Notice B</a></li>
          <li class="lcars-blue-bg tall large-gap"><a href="index-2.php">Notice C</a></li>
          <li class="lcars-tan-bg"></li>
          <li class="lcars-tan-bg tall"></li>
          <li class="lcars-tan-bg"></li>
        </ul>
      </div>

      <!-- CENTER SECTION
      =========================================================== -->
      <div class="lcars-column u-6-8">
        <div class="lcars-row">
          <div class="lcars-column u-1-2">
            <h3 class="title-of-page"><?php echo date("l, F d")."<br>";?></h3>
            <h6>Current: <?php echo round(($data['main']['temp'])+273.15);?> K (<?php echo round($data['main']['temp']); ?>&deg;C)</h6>
            <h6>High: <?php echo round(($data['main']['temp_max'])+273.15);?> K (<?php echo round($data['main']['temp_max']); ?>&deg;C)</h6>
            <h6>Low: <?php echo round(($data['main']['temp_min'])+273.15);?> K (<?php echo round($data['main']['temp_min']); ?>&deg;C)</h6>
            <h6>Conditions: <?php echo $data['weather'][0]['main']." (".$data['weather'][0]['description'].")";?></h6>
            <h6>Humidity: <?php echo $data['main']['humidity'] ?>%</h6>
            <h6>Wind speed: <?php echo $data['wind']['speed'] ?>km/h</h6>
          </div>
          <div class="lcars-column u-1-2 starfleet-logo-col"><img src="img/starfleet-logo.png">
          </div>
        </div>
      </div>

      <!-- RIGHT MENU
      =========================================================== -->
      <div class="lcars-column u-1-8">
        <ul class="lcars-menu right">
          <li class="lcars-tan-bg"><a href="#">Testing</a></li>
          <li class="lcars-tan-bg tall large-gap">Testing Large</li>
          <li class="lcars-blue-bg">Testing 2</li>
          <li class="lcars-blue-bg tall">Testing Large 2</li>
          <li class="lcars-blue-bg">Testing 3</li>
        </ul>
      </div>
    </div>

    <!-- BOTTOM ROW
    =========================================================== -->
    <div class="lcars-row spaced">

      <!-- BOTTOM LEFT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow left top lcars-tan-bg">
        <a href="weather.php">Weather</a>
      </div>

      <!-- BOTTOM DIVIDER AND BUTTONS
      =========================================================== -->
      <div class="lcars-column u-6-8 lcars-divider bottom lcars-tan-blue-divide">
        <div class="lcars-row">
          <div class="lcars-column u-1-2">
          </div>
          <div class="lcars-column u-1-2">
          </div>
        </div>
      </div>

      <!-- BOTTOM RIGHT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow right top lcars-blue-bg">
        <a href="#">Side Button 4</a>
      </div>
    </div>

  </div>

<script type="text/javascript">
  document.addEventListener("touchstart", function(){}, true);
</script>

</body>
</html>
