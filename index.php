<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>LCARS</title>
  <link rel="stylesheet" href="css/lcars.css">
  <link rel="stylesheet" href="css/custom.css">
  <style>
  html, body { background: black }
  </style>
</head>
<body>

  <div class="lcars-container">

    <!-- TOP ROW
    =========================================================== -->
    <div class="lcars-row spaced">

      <!-- TOP LEFT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow left bottom lcars-blue-bg">
        <a href="#">Notice A</a>
      </div>

      <!-- TOP DIVIDER AND BUTTONS
      =========================================================== -->
      <div class="lcars-column u-6-8 lcars-divider lcars-blue-tan-divide">
        <div class="lcars-row">
          <div class="lcars-column u-1-2">
            NOTICES
          </div>
          <div class="lcars-column u-1-2">
            <h1 class="right">3635</h1>
          </div>
        </div>
      </div>

      <!-- TOP RIGHT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow right bottom lcars-tan-bg">
        <a href="#">Side Button 2</a>
      </div>
    </div>

    <div class="lcars-row">

      <!-- LEFT MENU
      =========================================================== -->
      <div class="lcars-column u-1-8">
        <ul class="lcars-menu">
          <li class="lcars-blue-bg"><a href="index-1.php">Notice B</a></li>
          <li class="lcars-blue-bg tall large-gap"><a href="index-2.php">Notice C</a></li>
          <li class="lcars-tan-bg"></li>
          <li class="lcars-tan-bg tall"><a href="forecast.php"> Forcast</a></li>
          <li class="lcars-tan-bg"></li>
        </ul>
      </div>

      <!-- CENTER SECTION
      =========================================================== -->
      <div class="lcars-column u-6-8">
        <div class="lcars-row">
          <div class="lcars-column u-1-2">
            <h2 class="title-of-page">Hey Guys</h2>
            <h6><?php include('notice0.txt'); ?></h6>
          </div>
          <div class="lcars-column u-1-2 starfleet-logo-col"><img src="img/starfleet-logo.png">
          </div>
        </div>
      </div>

      <!-- RIGHT MENU
      =========================================================== -->
      <div class="lcars-column u-1-8">
        <ul class="lcars-menu right">
          <li class="lcars-tan-bg"><a href="#">Testing</a></li>
          <li class="lcars-tan-bg tall large-gap">Testing Large</li>
          <li class="lcars-blue-bg">Testing 2</li>
          <li class="lcars-blue-bg tall">Testing Large 2</li>
          <li class="lcars-blue-bg">Testing 3</li>
        </ul>
      </div>
    </div>

    <!-- BOTTOM ROW
    =========================================================== -->
    <div class="lcars-row spaced">

      <!-- BOTTOM LEFT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow left top lcars-tan-bg">
        <a href="weather.php">Weather</a>
      </div>

      <!-- BOTTOM DIVIDER AND BUTTONS
      =========================================================== -->
      <div class="lcars-column u-6-8 lcars-divider bottom lcars-tan-blue-divide">
        <div class="lcars-row">
          <div class="lcars-column u-1-2">
          </div>
          <div class="lcars-column u-1-2">
          </div>
        </div>
      </div>

      <!-- BOTTOM RIGHT ELBOW
      =========================================================== -->
      <div class="lcars-column u-1-8 lcars-elbow right top lcars-blue-bg">
        <a href="#">Side Button 4</a>
      </div>
    </div>

  </div>

</body>
</html>
